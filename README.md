# Audio Visualization

![Audio Visualization](https://gitlab.com/BadToxic/audio-visualization/raw/master/img/screenshot.png)

Simple blocks visualizing audio by increasing their y-scale and material color emission.
By default there are eight blocks listening to frequency bands of an audio source.
There is an audio peer instance calculating the necessary samples and bands to be used by many game objects.

by [BadToxic](http://badtoxic.de/wordpress)

## Get it for free
[Download from the Unity Asset Store](https://assetstore.unity.com/packages/tools/audio/audio-visualization-163282)

## Content

- *AudioPeer* prefab & C# script for fetching and calculating data from an AudioSource<br/>
- *AudioSpectrum* prefab & C# script creating 8 audio bumper<br/>
- *AudioBumper* prefab & C# script scaling and coloring to data by the AudioPeer<br/>
- *AudioVisualizationScene* for demonstration

## Requirements

_Shader_: Works on all platforms supported by Unity. *DX9* shader model *2.0*.*Audio file* (eg. mp3) to be visualized.

## Usage

Open the given scene and add an audio file to the existing _AudioSource_ and start the game.
You can use the _AudioPeer_'s _GetFrequencyBand(int freqIndex)_ method to get a frequency band consisting of several audio samples
or the _GetSample(int sampleIndex)_ method for a single sample. All band values are normalized floats between 0 and 1.
By default there are 512 samples that get devided into 8 frequency bands using their averages.
The values get buffered for a smoother experience.
The code is easy to adjust to be able to change the number of samples or bands.

## Support

Need help? Join my discord server: https://discord.gg/8QMCm2d

## Follow me

Support me with donations, likes and follows/subscriptions:
[Patreon](https://www.patreon.com/badtoxic)
[Instagram](https://www.instagram.com/xybadtoxic)
[Twitter](https://twitter.com/BadToxic)
[YouTube](https://www.youtube.com/user/BadToxic)
[Facebook](https://www.facebook.com/XyBadToxic)
[Twitch](https://www.twitch.tv/xybadtoxic)

## Demo

The audio visualization sadly can not work in [WebGL](https://badtoxic.gitlab.io/audio-visualization/) as the needed methods are not supported.